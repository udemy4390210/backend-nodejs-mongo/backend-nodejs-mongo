const express = require("express");
//Import path
const path = require("path");
const configViewEngine = (app) => {
  //app.set("views", path.join(__dirname, "./views")); ???
  app.set("views", path.join("./src", "./views"));
  app.set("view engine", "ejs");
  //Config static file
  app.use(express.static(path.join("./src", "public")));
};
module.exports = configViewEngine;
