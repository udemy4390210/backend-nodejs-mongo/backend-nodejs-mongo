//Import dotenv
require("dotenv").config(); // de dung process.env.etc...
//import mysql2
const mysql = require("mysql2/promise");
// //Create connection db
// const connection = mysql.createConnection({
//   host: process.env.DB_HOST,
//   user: process.env.DB_USER,
//   port: process.env.DB_PORT, //defaut: 3306
//   password: process.env.DB_PASSWORD, //default: emty
//   database: process.env.DB_NAME,
// });

// Create connection to connection pool
const connection = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  port: process.env.DB_PORT, //defaut: 3306
  password: process.env.DB_PASSWORD, //default: emty
  database: process.env.DB_NAME,
  waitForConnections: true,
  connectionLimit: 10,
  maxIdle: 10, // max idle connections, the default value is the same as `connectionLimit`
  idleTimeout: 60000, // idle connections timeout, in milliseconds, the default value 60000
  queueLimit: 0,
});

module.exports = connection;
