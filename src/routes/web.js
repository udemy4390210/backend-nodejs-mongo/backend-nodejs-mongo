//Serverside Rendering
const express = require("express");

//Import handler as object
const {
  getHomePage,
  getABC,
  hoidanIT,
  getCreateUserPage,
  getUpdateUserPage,
  getDeleteUserPage,
  postCreateUser,
  postUpdateUser,
  postDeleteUser,
} = require("../controllers/homeController");
const router = express.Router();

//Import MySQL
const mysql = require("mysql2");

// ** router.Method("/route, handler")
//Get Method
router.get("/homepage", getHomePage);
router.get("/getABC", getABC);
router.get("/hoidanIT", hoidanIT);
router.get("/create", getCreateUserPage);
router.post("/create-user", postCreateUser);
router.get("/update/:id", getUpdateUserPage);
router.post("/update", postUpdateUser);
router.get("/delete/:id", getDeleteUserPage);
router.post("/delete", postDeleteUser);

//Test Connection
//Create connecting to database
const conection = mysql.createConnection;
module.exports = router;
