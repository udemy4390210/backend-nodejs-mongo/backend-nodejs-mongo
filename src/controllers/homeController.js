//import connection from DB
const connection = require("../config/database");
const { getAllUser, getUserById, updateUserById, deleteUserById } = require("../services/CRUDServices");
let users = [];
// Handler Get Method
const getHomePage = async (req, res) => {
  let results = await getAllUser();
  console.log(results);
  return res.render("homepage.ejs", { listUsers: results });
};

const getCreateUserPage = (req, res) => {
  return res.render("createUser.ejs");
};
const getABC = (req, res) => {
  res.send("get ABC");
};
const hoidanIT = (req, res) => {
  return res.render("sample.ejs");
};

const getUpdateUserPage = async (req, res) => {
  const userId = req.params.id;
  let user = await getUserById(userId);
  console.log("user: ", user, "userId: ", userId);
  return res.render("updateUser.ejs", { userEdit: user });
};

const getDeleteUserPage = async (req, res) => {
  const userId = req.params.id;
  let user = await getUserById(userId);
  console.log("user: ", user, "userId: ", userId);
  return res.render("deleteUser.ejs", { userEdit: user });
};

const postUpdateUser = async (req, res) => {
  let { email, name, city } = req.body;
  let id = req.body.Id;
  await updateUserById(email, name, city, id);
  //res.send("Updated a new User successful ");
  return res.redirect("/homepage");
  console.log(id);
};

const postDeleteUser = async (req, res) => {
  let id = req.body.Id;
  await deleteUserById(id);
  //res.send("Delete User successful ");
  return res.redirect("/homepage");
  console.log(id);
};
const postCreateUser = async (req, res) => {
  console.log(req.body);
  let { email, name, city } = req.body;
  // Send data to database
  // connection.query(
  //   `INSERT INTO Users  (email, name , city)
  //   VALUES (?, ?, ?)`, // truyen dong du lieu
  //   [email, name, city],
  //   function (err, results) {
  //     console.log(results);
  //   }
  // );
  // connection.query("SELECT * FROM Users u ", function (err, results, fields) {
  //   console.log(results); // results contains rows returned by server
  // });
  //const [results, fields] = await connection.query("SELECT * FROM Users u ");

  const [results, fields] = await connection.query(
    `INSERT INTO Users
    (email, name , city) VALUES (?, ?, ?)`, // truyen dong du lieu
    [email, name, city]
  );
  res.send("Created a new User successful ");
  console.log(results);
};
// Many handlers so export to object
module.exports = {
  getHomePage,
  getABC,
  hoidanIT,
  postCreateUser,
  getCreateUserPage,
  getUpdateUserPage,
  getDeleteUserPage,
  postUpdateUser,
  updateUserById,
  postDeleteUser,
};
