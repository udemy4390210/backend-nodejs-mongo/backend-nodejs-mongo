//import express from "express";
const express = require("express");

//import connection from database;
const connection = require("./config/database");

//Declare app
const app = express();

//import Router;
const webRoute = require("./routes/web");

//Import dotenv
require("dotenv").config();

//Import view engine
const configViewEngine = require("./config/viewEngine");

//Config template engine
configViewEngine(app);

//Config Req.body
app.use(express.json()); // for json
app.use(express.urlencoded({ extended: true })); //for form data
//Declare port
const port = process.env.PORT || 8888;

//Declare hostname
const hostname = process.env.HOST_NAME;

//Connect Data from DB
// connection.query("SELECT * FROM Users u ", function (err, results, fields) {
//   console.log(results); // results contains rows returned by server
// });

//Declare ROUTE
app.use("/", webRoute); //them j thi them nhung coi chung bug

//Listen on port
app.listen(port, hostname, () => {
  console.log(`App's running on ${port}`);
});
