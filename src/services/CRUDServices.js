const connection = require("../config/database");
const getAllUser = async () => {
  let [results, fields] = await connection.query("select * from Users");
  return results;
};

const getUserById = async (userId) => {
  let [results, fields] = await connection.query(`select * from Users where id = ?`, [userId]);
  //Check if userId exist : as req.parms.id return an object,
  //if object != {}, get first element, if object = {}, return {}
  let user = results && results.length > 0 ? results[0] : {};
  // truyen bien user qua userId
  return user;
};
const updateUserById = async (email, city, name, id) => {
  const [results, fields] = await connection.query(
    // `UPDATE Users
    // SET (email, name , city) VALUES (?, ?, ?) WHERE id = ?`, // truyen dong du lieu
    // [email, name, city, id] KO XAI DC INSERT THI DC
    `UPDATE Users
    SET email = ?, city = ?, name = ?
    WHERE id = ?`,
    [email, city, name, id]
  );
};

const deleteUserById = async (id) => {
  const [results, fields] = await connection.query(
    `DELETE FROM Users
    WHERE id = ?`,
    [id]
  );
};
module.exports = {
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById,
};
